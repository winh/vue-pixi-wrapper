import Vue from 'vue'
const pkg = require('../package.json')

Vue.mixin({
  computed: {
    assetsUrl() {
      return `/${pkg.name}/pixijs-examples/`
    }
  }
})
