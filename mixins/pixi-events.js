export default {
  methods: {
    addPixiEventListeners(pixiObject, eventNames) {
      eventNames.forEach(name => {
        if (!this.$listeners[name]) {
          // avoid unnecessary listeners
          return
        }

        pixiObject.on(name, this.handlePixiEvent)
      })
    },

    handlePixiEvent(event) {
      const { type } = event
      this.$emit(type, event)
    }
  }
}
